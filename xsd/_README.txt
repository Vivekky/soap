The contents of this directory contains MODIFIED OpenTravel messages.

The original messages can be downloaded from http://www.opentravel.org.

The only difference between these mesasges and the original OpenTravel messages are that
the schema files implement an xmlns and a targetNamespace of "http://www.opentravel.org/OTA/2003/05".

This approach was necessary for htng-created schemas and messages to live in the
"http://htng.org/2010A" namespace and avoid duplication of generated entities across
the two specifications.  This has been submitted to OpenTravel for future adoption.
